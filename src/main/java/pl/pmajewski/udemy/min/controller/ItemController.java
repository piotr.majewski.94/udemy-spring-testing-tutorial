package pl.pmajewski.udemy.min.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.pmajewski.udemy.min.model.Item;
import pl.pmajewski.udemy.min.service.ItemService;

@RestController
public class ItemController {

	@Autowired
	private ItemService itemService;
	
	@GetMapping("/dummy-item")
	public Item getItem() {
		return new Item(1, "Ball", 10, 10);
	}
	
	@GetMapping("/item-from-business-service")
	public Item getItemFromService() {
		return itemService.getItemHardcoded();
	}
	
	@GetMapping("/find-all")
	private List<Item> findAll() {
		return itemService.findAll();
	}
}
