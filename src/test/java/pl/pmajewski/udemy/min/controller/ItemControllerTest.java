package pl.pmajewski.udemy.min.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import pl.pmajewski.udemy.min.model.Item;
import pl.pmajewski.udemy.min.service.ItemService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ItemController.class)
class ItemControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private ItemService itemService;
	
	@Test
	void getItemTest() throws Exception {
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders
			.get("/dummy-item")
			.accept(MediaType.APPLICATION_JSON);
	
		mockMvc.perform(request)
			.andExpect(status().isOk())
			.andExpect(content().json("{\"id\":1,\"name\":\"Ball\",\"price\":10,\"quantity\":10}"))
			.andReturn();
	}
	
	@Test
	void getItemFromServiceTest() throws Exception {
		when(itemService.getItemHardcoded()).thenReturn(new Item(1, "Ball", 10, 10));
		
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders
				.get("/item-from-business-service")
				.accept(MediaType.APPLICATION_JSON);
		
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(content().json("{\"id\":1,\"name\":\"Ball\",\"price\":10,\"quantity\":10}"))
				.andReturn();		
	}
	
	@Test
	void getItemFromDB() throws Exception {
		when(itemService.findAll()).thenReturn(Arrays.asList(new Item(1, "Ball1", 10, 10), new Item(2, "Ball2", 20, 20)));
		
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders
				.get("/find-all")
				.accept(MediaType.APPLICATION_JSON);
		
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(content().json("[{id:1, name:Ball1, price:10, quantity:10}, {id:2, name:Ball2, price:20, quantity:20}]"))
				.andReturn();		
	}
}
