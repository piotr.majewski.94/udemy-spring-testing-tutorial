package pl.pmajewski.udemy.min.controller;

import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import pl.pmajewski.udemy.min.model.Item;
import pl.pmajewski.udemy.min.repository.ItemRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class ItemControllerMockIT {

	@Autowired
	private TestRestTemplate restTemplate;
	
	@MockBean
	private ItemRepository itemRepository;
	
	@Test
	void findAllTest() throws JSONException {
		when(itemRepository.findAll()).thenReturn(Arrays.asList(new Item(1, "", 1, 1), new Item(2, "", 1, 1), new Item(3, "", 1, 1)));
		
		String response = this.restTemplate.getForObject("/find-all", String.class);
		JSONAssert.assertEquals("[{id: 1}, {id: 2}, {id: 3}]", response, false);
	}

}
