package pl.pmajewski.udemy.min;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

class JsonAssertTest {

	@Test
	void test() throws JSONException {
		String expected = "{\"id\":1,\"name\":\"Ball\",\"price\":10,\"quantity\":10}";
		JSONAssert.assertEquals(expected, expected, false);
	
	}
}
