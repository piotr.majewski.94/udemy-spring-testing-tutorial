package pl.pmajewski.udemy.min.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class SomeServiceTest {

	@Test
	@DisplayName("Sum 1,2,3 - without mock")
	void testCalcSum() {
		SomeService service = new SomeService();
		int actual = service.calcSumAll(new int[] {1, 2, 3});
		int expected = 6;
		assertEquals(expected, actual);
	}
	
	@Test
	@DisplayName("Empty table - without mock")
	void testCalcSumEmpty() {
		SomeService service = new SomeService();
		int actual = service.calcSumAll(new int[] { });
		int expected = 0;
		assertEquals(expected, actual);
	}

}
