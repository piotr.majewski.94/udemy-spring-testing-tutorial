package pl.pmajewski.udemy.min.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import pl.pmajewski.udemy.min.repository.NumberRepository;

class SomeServiceDatabaseTest {

	@Test
	void testCalcSumAllDatabase() {
		SomeService service = new SomeService();
		service.setNumberRepo(new NumberRepositoryStub());;
		int actual = service.calcSumAllDatabase();
		int expected = 6;
		assertEquals(expected, actual);
	}

	class NumberRepositoryStub implements NumberRepository {

		@Override
		public int[] getAll() {
			return new int[] {1, 2, 3};
		}
	}
}
